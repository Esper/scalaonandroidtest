package object Log {

  def v(msg: String)(implicit tag: String): Unit = android.util.Log.v(tag, msg)

  def d(msg: String)(implicit tag: String): Unit = android.util.Log.d(tag, msg)

  def i(msg: String)(implicit tag: String): Unit = android.util.Log.i(tag, msg)

  def w(msg: String)(implicit tag: String): Unit = android.util.Log.w(tag, msg)

  def e(msg: String)(implicit tag: String): Unit = android.util.Log.e(tag, msg)

}