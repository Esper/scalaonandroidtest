import java.util

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.{Button, Toast}


package object Conversions {

  implicit class ScalaButton(button: Button) {
    def onClick(f: View => Unit): Unit = {
      button.setOnClickListener(new OnClickListener {
        override def onClick(p1: View): Unit = f(p1)
      })
    }
  }

  implicit class ToastableString(msg: String) {
    def toast(duration: Int = Toast.LENGTH_LONG)(implicit ctx: Context): Unit = Toast.makeText(ctx, msg, duration).show()
  }
}

package object Mixins {

  trait ImplicitContext {
    this: Activity =>

    implicit lazy val ctx = getApplicationContext
  }

  trait ActivityPimping {
    this: Activity =>

    def findResource[T](id: Int)(implicit ctx: Context, m: Manifest[T]): T = m.toString() match {
      case "java.lang.String" =>
        ctx.getString(id).asInstanceOf[T]

      case "java.util.ArrayList[java.lang.String]" =>
        val arrayAsList = util.Arrays.asList(ctx.getResources.getStringArray(id))
        new util.ArrayList(arrayAsList).asInstanceOf[T]

      case "Int" =>
        ctx.getString(id).toInt.asInstanceOf[T]

      case other =>
        throw new UnableToResolveResource(ofType = other, id = id)
    }
  }

  trait SimpleLifecycle extends Activity {

    override def onCreate(savedInstanceState: Bundle): Unit = {
      super.onCreate(savedInstanceState)

      onCreate()
    }

    def onCreate(): Unit = {}
  }
}

class UnableToResolveResource(ofType: String, id: Int) extends Throwable

