package com.example.felix.scalaonandroidtest

import android.widget.Button

import Conversions._
import Mixins._

class TestActivity extends SimpleLifecycle with ImplicitContext with ActivityPimping {

  implicit val TAG = "SCALALOG"

  override def onCreate(): Unit = {
    setContentView(R.layout.activity_main)

    Log.d("SHIT WORKS IN SCALA, YO!")

    val button = findResource[Button](R.id.action_button)
    button.onClick { _ => "Toast dat shit!".toast() }
  }
}